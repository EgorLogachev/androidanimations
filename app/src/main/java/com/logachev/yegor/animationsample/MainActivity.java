package com.logachev.yegor.animationsample;

import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Slide;
import android.view.Gravity;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int[] SRC_IMGS_RESOURCES = {R.drawable.car_0, R.drawable.car_1, R.drawable.car_2,
            R.drawable.car_3, R.drawable.car_4, R.drawable.car_5, R.drawable.car_6, R.drawable.car_7,
            R.drawable.car_8, R.drawable.car_9, R.drawable.car_10, R.drawable.car_11};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.btnPropertyAnimations).setOnClickListener(this);
        findViewById(R.id.btnTransitionAnimations).setOnClickListener(this);
        findViewById(R.id.btnViewPagerAnimation).setOnClickListener(this);
        findViewById(R.id.btnListAnimation).setOnClickListener(this);
        findViewById(R.id.btnControllersTransitions).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnPropertyAnimations:
                onPropertyAnimationsClick();
                break;

            case R.id.btnTransitionAnimations:
                onTransitionAnimationsClick();
                break;

            case R.id.btnViewPagerAnimation:
                onViewPagerAnimationClick();
                break;

            case R.id.btnListAnimation:
                onListAnimationClick();
                break;

            case R.id.btnControllersTransitions:
                onControllersTransitionsClick();
                break;
        }
    }

    private void startActivity(Class<? extends AppCompatActivity> cls) {
        startActivity(new Intent(this, cls));
    }

    private void onPropertyAnimationsClick() {
        startActivity(PropertyAnimActivity.class);
    }

    private void onTransitionAnimationsClick() {
        startActivity(TransitionAnimActivity.class);
    }

    private void onViewPagerAnimationClick() {
        startActivity(ViewPagerActivity.class);
    }

    private void onListAnimationClick() {
        startActivity(ViewAnimActivity.class);
    }

    private void onControllersTransitionsClick() {
        getWindow().setExitTransition(new Slide(Gravity.TOP));
        Bundle bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(this).toBundle();
        startActivity(new Intent(this, ScreenTransitionsActivity.class), bundle);
    }
}
